#include "i2c_L4.h"

//-------------------------------------------------------------

I2C::I2C(i2c_config conf) : i2c_conf(conf) {
    /*Enable I2c clock */
    i2c_conf.rcc_clock |= i2c_conf.rcc_clock_enable_bit;

    /* Timing register value is computed with the STM32CubeMX Tool,
    * Fast Mode @400kHz with I2CCLK = 80 MHz,
    * rise time = 100ns, fall time = 10ns
    * Timing Value = (uint32_t)0x00F02B86
    */
    i2c_conf.i2c->TIMINGR = (uint32_t)0x00F02B86;
    /*clock stretching enabled by default*/
    /*Enable Peripheral*/
    i2c_conf.i2c->CR1|=I2C_CR1_PE;

    /*Set slave @*/
    i2c_conf.i2c->CR2|= i2c_conf.slave_address<<I2C_CR2_SADD_Pos;




    /*config nbytes*/


    /*Config I2c Dma*/
    i2c_conf.i2c->CR1|=I2C_CR1_TXDMAEN|I2C_CR1_RXDMAEN;
    /*When all data are transferred using DMA, the DMA must be
    initialized before setting the START bit. The end of transfer is managed with the
    NBYTES counter. Refer to Master transmitter on page 1295 */


}
