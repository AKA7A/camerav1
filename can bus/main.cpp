#include <mdhal/Time.hpp>
#include <mdhal/can_bus/can_bus.hpp>
#include <mdhal/gpio/Gpio.hpp>

can_bus can1;
Gpio blue_led{{GPIOB,7}};
int main()
{
    can1.gpio_config();
    can1.can1_init();
    can1.nvic_init();
    can1.can1_transmit();
    blue_led.set(true);
    while (1)
    {}
    return 0;
}

extern "C" void CAN1_RX0_IRQHandler()
{
    can1.can1_receive();

}