#pragma once
#include <mdhal/gpio/Gpio.hpp>

struct usart_setup {
    USART_TypeDef * const can_hw;

    volatile uint32_t &APBclock;
    const uint32_t APBbit;

    Gpio::Pin tx;
    Gpio::Pin rx;
    const Gpio::AlternateFunction alternateFunction;

    const IRQn_Type interrupt;
    const uint8_t interruptPriority;
};
class can_bus {
public:
    void gpio_config(void);

    void can1_init(void);

    void can1_transmit(void);

    void can1_receive(void);

    void nvic_init();
};



