#include "can_bus.hpp"
void can_bus::gpio_config(void) {
Gpio canRx{{GPIOB, 8}, Gpio::Function::ALTERNATE, Gpio::Type::PUSH_PULL, Gpio::Pull::UP, Gpio::Speed::HIGH,
           Gpio::AlternateFunction::AF9};
Gpio canTx{{GPIOB, 9}, Gpio::Function::ALTERNATE, Gpio::Type::PUSH_PULL, Gpio::Pull::UP, Gpio::Speed::HIGH,
           Gpio::AlternateFunction::AF9};
}
uint32_t txCounter = 0;


void can_bus::nvic_init()
{
    NVIC_EnableIRQ(CAN1_RX0_IRQn);
    NVIC_SetPriority(CAN1_RX0_IRQn,0);
    NVIC_EnableIRQ(CAN1_RX1_IRQn);
    NVIC_SetPriority(CAN1_RX1_IRQn,0);
}
void can_bus::can1_init(void) {

    /* Configure CAN1 */
    /* Enable CAN1 clock */
    RCC->APB1ENR1 = RCC_APB1ENR1_CAN1EN;

    /* Configuration after reset:
     * **************************
     * CAN reception/transmission frozen during debug.
     * Time Triggered Communication mode disabled.
     * The Sleep mode is left on software request.
     * Automatic retransmission.
     * Receive FIFO not locked on overrun.
     * */

    /* Set automatic bus-off management */
    /*if trans counter >128  --> bus off(no transmisssion or recep) so to transmit or receive again
     *the state of bus must go to error active state by receiving 128 consecutive recessive bits (CAN standards)
     *if the buss of management bit is set the bus state will be switched automatically if it's not set
     *the bus will be in off state until it's requested by software to enter error active state*/

    CAN1->IER|=CAN_IER_FMPIE0;

    // FiFo1 message peding interrupt enable
    CAN1->IER|=CAN_IER_FMPIE1;

    /* Set automatic bus-off management      /(MAN    11 consec rec )*/
    CAN1->MCR |= CAN_MCR_ABOM;




    /* Set transmit FIFO priority driven by the request order ,the other option is to drive by the identifier
     * lowest identifier --> highest priority */
    CAN1->MCR |= CAN_MCR_TXFP;

    /* Initialization request */
    CAN1->MCR |= CAN_MCR_INRQ;

    /* Request to exit Sleep mode */
    CAN1->MCR &= ~CAN_MCR_SLEEP;

    /* Wait for initialization mode */
    while((CAN_MSR_SLAK == (CAN_MSR_SLAK & CAN1->MSR))
          || (CAN_MSR_INAK != (CAN_MSR_INAK & CAN1->MSR)))
    {
        /* Do nothing until initialization mode is entered */
    }

    /*NART*/
//    CAN1->MCR|=CAN_MCR_NART;


    /* Setup timing parameters,
     * BaudRate = 1 Mbps, BRP[9:0] = 4, TS1[3:0] = 6, TS2[2:0] = 0, SJW[1:0] = 0
     * tq = (BRP[9:0] + 1) x tPCLK = 5 * 0,0222 = 0,111 us
     * tBS1 = tq x (TS1[3:0] + 1) = 0.111 * 7 = 0.777 us
     * tBS2 = tq x (TS2[2:0] + 1) = 0.111 * 1 = 0.111 us
     * NominalBitTime = 1 × tq + tBS1 + tBS2 = 0.111 + 0.777 + 0.111 = 0,9999 (1) us
     * BaudRate = 1 / NominalBitTime = 1 MBits/s */

    // select the BTR reg using the MCU freq-->
    CAN1->BTR = 0x001c0009;//-->
    CAN1->BTR |=CAN_BTR_LBKM;
    /* Reset identifier register, no extension, no remote */
    CAN1->sTxMailBox[0].TIR = 0;

    /* Set CAN standard identifier = 0x10 */
    CAN1->sTxMailBox[0].TIR |= 0x2000000;

    /* Set CAN frame length to 8 */
    CAN1->sTxMailBox[0].TDTR &= ~CAN_TDT0R_DLC;
    CAN1->sTxMailBox[0].TDTR |= 0x00000001;

    /* Reset transmit data registers  */
    CAN1->sTxMailBox[0].TDLR = 0;
    CAN1->sTxMailBox[0].TDHR = 0;

    /* Filter setup,
     * Filter 1 0x25           Turn on/off green led
     * Filter 2 (0x10 - 0x1F)  Turn on/off green led
     * Filter 3 0x15           Turn on/off red led
     * Filter 4 0x20 - 0x2F    Turn on/off red led
     *
     * After reset filters in init mode and for CAN1
     * filter until number 13. Filter scale dual 16-bit */

    /* Set Filter init mode */
    CAN1->FMR |= CAN_FMR_FINIT;

    /* Set filer mode, 1 and 3 list, 2 and 4 mask */
    CAN1->FM1R &= ~(CAN_FM1R_FBM2 | CAN_FM1R_FBM4);
    CAN1->FM1R |= (CAN_FM1R_FBM1 | CAN_FM1R_FBM3);

    /* Set FIFO 0 for all filters */
    CAN1->FFA1R &= ~(CAN_FFA1R_FFA1 | CAN_FFA1R_FFA2
                     | CAN_FFA1R_FFA3 | CAN_FFA1R_FFA4);

    /* Filter mapping
     *   15-8          7-5    4   3    2-0
     * STID[10:3] STID[2:0] RTR IDE EXID[17:15]
     * */

    /* Filter 0 Deactivated ID Mask (default) (default 16-bit) No. 0, 1 */

    /* Set filter 1, list mode with ID = 0x25 */
    /*you must configure the 2*32 regs  because the other reg  may contain  0s pr 1s*/
    CAN1->sFilterRegister[1].FR1 = 0x04A004A0; /* No. 2, 3 */
    CAN1->sFilterRegister[1].FR2 = 0x04A004A0; /* No. 4, 5 */

    /* Set filter 2, mask mode with ID = 0x10:0x1F */
    /*32 bits reg ID
     * 32 bits Mask
     * 5<<x10   --> Id reg
     * for the Mask reg we want a range from 0x10 to 0x1F ,ps:we work with 2* (2* 16) bits filters
     * so the first 16 bit ifentifier contains the ID    5<<0x10
     * the second one contaqins the Mask you put the ID and the 0xF ->0x00 (don't care bits and the rest is 1  (must match))
     *
     * range start xor range end --> range --> not
     * range start -->ID
     * to check the size use size of then make & with FFFF to check if its 2 or three byte size...
     *
     * */

    /*!!! to check the filter number when we receive please check the ref manual (Example of filter numbering)*/
    CAN1->sFilterRegister[2].FR1 = 0xFE1F0200; /* No. 6 */
    CAN1->sFilterRegister[2].FR2 = 0xFE1F0200; /* No. 7 */

    /* Set filter 3, list mode with ID = 0x15 */
    CAN1->sFilterRegister[3].FR1 = 0x02A002A0; /* No. 8, 9 */
    CAN1->sFilterRegister[3].FR2 = 0x02A002A0; /* No. 10, 11 */

    /* Set filter 4, mask mode with ID = 0x20:0x2F */
    CAN1->sFilterRegister[4].FR1 = 0xFE1F0400; /* No. 12 */
    CAN1->sFilterRegister[4].FR2 = 0xFE1F0400; /* No. 13 */

    /* Activate filters */
    CAN1->FA1R |= (CAN_FA1R_FACT1 | CAN_FA1R_FACT2
                   | CAN_FA1R_FACT3 | CAN_FA1R_FACT4);

    /* Active filters mode */
    CAN1->FMR &= ~CAN_FMR_FINIT;

    /*enable mbox0 reception interrupt*/
    CAN1->IER|=CAN_IER_FMPIE0;

    /* Request normal mode */
    CAN1->MCR &= ~(CAN_MCR_INRQ | CAN_MCR_SLEEP);

    /* Wait for initialization mode */
    while((CAN_MSR_SLAK != (CAN_MSR_SLAK & CAN1->MSR))
          & (CAN_MSR_INAK != (CAN_MSR_INAK & CAN1->MSR)))
    {
        /* Do nothing until initialization mode is entered */
    }
}

void can_bus::can1_transmit(void) {
    /* Check if transmission mailbox empty */
    if(CAN_TSR_TME0 == (CAN_TSR_TME0 & CAN1->TSR))
    {
        /* Increment TX counter */
        txCounter++;

        /* Set TX data */
        CAN1->sTxMailBox[0].TDLR = txCounter;

        /* Transmission request */
        CAN1->sTxMailBox[0].TIR |= CAN_TI0R_TXRQ;

        while((CAN1->TSR&CAN_TSR_TME0)!=CAN_TSR_TME0);
    }
    else
    {
        /* Do nothing */
    }
}

void can_bus::can1_receive(void) {
    uint8_t rxData;
    uint8_t dlc;
    uint8_t filterIndex;

    /* Check FIFO message pending */
    if(0 != (CAN_RF0R_FMP0 & CAN1->RF0R))
    {
        /* FIFO not empty, read data */
        rxData = (uint8_t)(CAN1->sFIFOMailBox[0].RDLR);

        /* Get filter match index */
        filterIndex = (uint8_t)(CAN1->sFIFOMailBox[0].RDTR >> 8);

        /* Get data length code */
        dlc = (uint8_t)(CAN1->sFIFOMailBox[0].RDTR);

        /* Check filter index */
        if((2 <= filterIndex) && (7 >= filterIndex) && (1 == dlc))
        {
            /* Green led control */
            if(0x0F == rxData)
            {
                /* Turn off green led */
//                GPIO_TurnOFF_LED(EVAL_GREEN_LED);
            }
            else if (0x1F == rxData)
            {
                /* Turn on green led */
//                GPIO_TurnON_LED(EVAL_GREEN_LED);
            }
            else
            {
                /* Do nothing, invalid led control command */
            }
        }
        else if ((8 <= filterIndex) && (13 >= filterIndex) && (1 == dlc))
        {
            /* Red led control */
            if(0x0F == rxData)
            {
                /* Turn off red led */
//                GPIO_TurnOFF_LED(EVAL_RED_LED);
            }
            else if (0x1F == rxData)
            {
                /* Turn on red led */
//                GPIO_TurnON_LED(EVAL_RED_LED);
            }
            else
            {
                /* Do nothing, invalid led control command */
            }
        }
        else
        {
            /* Do noting, filter index or DLC not valid */
        }

        /* Release FIFO for the next message */
        CAN1->RF0R |= CAN_RF0R_RFOM0;
    }
    else
    {
        /* Do nothing, no new data */
    }
}
