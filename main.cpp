#include <cstring>
#include <mdhal/Time.hpp>

#include <mdhal/i2c/i2c_L4.h>
uint8_t      aReceiveBuffer[0xF]       = {0};
__IO uint8_t ubReceiveIndex            = 0;
I2C device;

uint8_t buff[6]="Hello";
Gpio led{{GPIOB,7}};
Gpio tstled{{GPIOC,7}};
Gpio button{{GPIOC,13},Gpio::Function::INPUT,Gpio::Type::PUSH_PULL,Gpio::Pull::DOWN};
uint32_t  clock;
int main()
{

device.DMA_TX_Config();
device.DMA_Transmit(buff,sizeof(buff));
device.i2c_slave_config(0x5A);
device.i2c_master_config();
while(!button.get())
{
    led.toggle();
    time().delayMs(300);
}
led.set(false);
device.handle_master(sizeof(buff),0x5A);

while(1)
{


}

    return 0;
}

extern  "C" void DMA1_Channel2_IRQHandler()
{
    if(DMA_ISR_TCIF2 == (DMA_ISR_TCIF2 & DMA1->ISR))
    {
        DMA1->IFCR = (DMA_IFCR_CGIF2 | DMA_IFCR_CTCIF2 | DMA_IFCR_CHTIF2
                      | DMA_IFCR_CTEIF2);
        led.set(true);
    }
    else  if(DMA_ISR_TEIF2 == (DMA_ISR_TEIF2 & DMA1->ISR))
    {
        /*Error*/
        tstled.set(true);
    }

}
uint8_t  addressrec;
extern  "C" void I2C1_EV_IRQHandler()
{

    if((I2C1->ISR &I2C_ISR_ADDR)==I2C_ISR_ADDR )
    {
      if(I2C1->ISR  >> I2C_ISR_ADDCODE_Pos << 1 ==0x5A)
      {
          if(!(I2C1->ISR & I2C_ISR_DIR))
          {

//              tstled.set(true);
              /*clear flag*/
              I2C1->ICR|=I2C_ICR_ADDRCF;
              /*Enable Receive interrupt*/
              I2C1->CR1|=I2C_CR1_RXIE;
          }
          else
          {
              /*clear flag*/
              I2C1->ICR|=I2C_ICR_ADDRCF;
              //--* error detected --> error callback
              tstled.set(true);
          }
      }

      else
      {
          /*clear flag*/
          I2C1->ICR|=I2C_ICR_ADDRCF;
          //--* error detected --> error callback
          tstled.set(true);
      }




    }

        /* Check RXNE flag value in ISR register */
    else if((I2C1->ISR &I2C_ISR_RXNE)==I2C_ISR_RXNE)
    {
        /* Read character in Receive Data register.
        RXNE flag is cleared by reading data in RXDR register */
        aReceiveBuffer[ubReceiveIndex++] = I2C1->RXDR;
    }
/* Check STOP flag value in ISR register */
    else if((I2C1->ISR &I2C_ISR_STOPF)==I2C_ISR_STOPF)
    {
        /*clear flag*/
        I2C1->ICR|=I2C_ICR_STOPCF;
        led.set(true);
    }
    else
    {
        tstled.set(true);
        /*Error detected*/
    }


}

extern  "C" void I2C1_ER_IRQHandler()
{

    tstled.set(true);
}