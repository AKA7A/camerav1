#pragma once
#include "mdhal/gpio/Gpio.hpp"
#include <cstddef>
#include <cstdint>

enum class i2c_speed_mode {
    StandardMode, FastMode, FastModePlus
};
enum class i2c_addressing_mode {
    Bits_7, Bits_10
};

enum class i2c_operation_mode
{Master,Slave};
struct i2c_config {

    volatile uint32_t &rcc_clock;
    const uint32_t rcc_clock_enable_bit;
    I2C_TypeDef *i2c;
    Gpio::Pin sda;
    Gpio::Pin scl;
    Gpio::AlternateFunction alternateFunction;
    i2c_speed_mode speed;
    i2c_addressing_mode addresing;
    uint8_t  slave_address;
    i2c_operation_mode operation;
};


class I2C {
protected:
    i2c_config i2c_conf;

private:
    Gpio scl{i2c_conf.scl, Gpio::Function::ALTERNATE, Gpio::Type::PUSH_PULL, Gpio::Pull::NO, Gpio::Speed::VERY_HIGH,
             i2c_conf.alternateFunction};


public:
    I2C(i2c_config config);

};