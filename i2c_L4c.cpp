#include "i2c_L4.h"

// I2C3 tx dma1 cxs 3 channel  2 rx 3
void I2C::DMA_Transmit(const uint8_t *pBuffer, uint8_t size) {
    /* Check null pointers */
    if (nullptr != pBuffer) {
        /* Wait until DMA1_Stream4 is disabled */
        while (DMA_CCR_EN == (DMA_CCR_EN & DMA1_Channel2->CCR)) {
            /* Do nothing, the enable flag shall reset
             * when DMA transfer complete */
        }

        /* Set memory address */
        DMA1_Channel2->CMAR = (uint32_t) pBuffer;

        /* Set number of data items */
        DMA1_Channel2->CNDTR = size;

        /* Clear all interrupt flags */
        DMA1->IFCR = (DMA_IFCR_CGIF2 | DMA_IFCR_CTCIF2 | DMA_IFCR_CHTIF2
                      | DMA_IFCR_CTEIF2);





    } else {
        /* Null pointers, do nothing */
    }
}



void I2C::DMA_TX_Config(void) {
    /* Enable DMA clock in RCC */
    RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;

    /* Make sure that the DMA1_Stream2 is disabled */
    if (DMA_CCR_EN == (DMA_CCR_EN & DMA1_Channel2->CCR)) {
        /* DMA1_Stream4 is enabled, shall be disabled first */
        DMA1_Channel2->CCR &= ~DMA_CCR_EN;

        /* Wait until EN bit is cleared */
        while (DMA_CCR_EN == (DMA_CCR_EN & DMA1_Channel2->CCR)) {
            /* Do nothing until EN bit is cleared */
        }
    } else {
        /* Do nothing, DMA1_Stream4 is not enabled */
    }

    /* Select the DMA channel 3 in CHSEL[2:0] in the DMA_SxCR */
    DMA1_CSELR->CSELR |= (3U << DMA_CSELR_C2S_Pos);

    /* Select stream priority very high */
    DMA1_Channel2->CCR |= DMA_CCR_PL;

    /* Select the data transfer direction memory-to-peripheral */
    DMA1_Channel2->CCR &= ~DMA_CCR_DIR;
    DMA1_Channel2->CCR |= DMA_CCR_DIR;

    /* Select memory and peripherals sizes byte (8-bit) */
    DMA1_Channel2->CCR &= ~DMA_CCR_MSIZE;
    DMA1_Channel2->CCR &= ~DMA_CCR_PSIZE;



    /* Select memory incremented mode, peripheral shall has fixed address */
    DMA1_Channel2->CCR |= DMA_CCR_MINC;

    /* Enable DMA transfer complete interrupt */
    DMA1_Channel2->CCR |= DMA_CCR_TCIE;
    DMA1_Channel2->CCR |= DMA_CCR_TEIE;

    /* Set peripheral address */
    DMA1_Channel2->CPAR = (uint32_t) &I2C3->TXDR;


    /* (2) Configure NVIC for DMA1 */
    NVIC_SetPriority(DMA1_Channel2_IRQn, 0);
    NVIC_EnableIRQ(DMA1_Channel2_IRQn);
}



void I2C::i2c_slave_config(uint8_t saddress)
{
    /*configure scl and sda pins*/
    Gpio scl{{GPIOB, 8},Gpio::Function::ALTERNATE,Gpio::Type::OPEN_DRAIN,Gpio::Pull::UP,Gpio::Speed::HIGH,Gpio::AlternateFunction::AF4};
    Gpio sda{{GPIOB, 9},Gpio::Function::ALTERNATE,Gpio::Type::OPEN_DRAIN,Gpio::Pull::UP,Gpio::Speed::HIGH,Gpio::AlternateFunction::AF4};
/* Enable I2C3 clock */
    RCC->APB1ENR1 |= RCC_APB1ENR1_I2C1EN;

    /* Configure Event IT:
 *  - Set priority for I2C1_EV_IRQn
 *  - Enable I2C1_EV_IRQn
 */
    NVIC_SetPriority(I2C1_EV_IRQn, 0);
    NVIC_EnableIRQ(I2C1_EV_IRQn);




    /* Configure Error IT:
     *  - Set priority for I2C1_ER_IRQn
     *  - Enable I2C1_ER_IRQn
     */
    NVIC_SetPriority(I2C1_ER_IRQn, 0);
    NVIC_EnableIRQ(I2C1_ER_IRQn);
    /* Make sure the peripheral is disabled for clock configuration */
    I2C1->CR1 &= ~I2C_CR1_PE;

    /* Timing register value is computed with the STM32CubeMX Tool,
    * Fast Mode @400kHz with I2CCLK = 80 MHz,
    * rise time = 100ns, fall time = 10ns
    * Timing Value = (uint32_t)0x00F02B86
    */
    I2C1->TIMINGR = (uint32_t) 0x00F02B86;




    I2C1->OAR1&=~I2C_OAR1_OA1;

    I2C1->OAR1 |=saddress<<I2C_OAR1_OA1_Pos;
    I2C1->OAR1 |=I2C_OAR1_OA1EN;


    /*Enable Peripheral*/
    I2C1->CR1 |= I2C_CR1_PE;


    I2C1->CR1|=I2C_CR1_ADDRIE|I2C_CR1_NACKIE|I2C_CR1_STOPIE|I2C_CR1_ERRIE;
}



void I2C::i2c_master_config() {
/*configure scl and sda pins*/
    Gpio scl{{GPIOC, 0},Gpio::Function::ALTERNATE,Gpio::Type::OPEN_DRAIN,Gpio::Pull::UP,Gpio::Speed::HIGH,Gpio::AlternateFunction::AF4};
    Gpio sda{{GPIOC, 1},Gpio::Function::ALTERNATE,Gpio::Type::OPEN_DRAIN,Gpio::Pull::UP,Gpio::Speed::HIGH,Gpio::AlternateFunction::AF4};
/* Enable I2C3 clock */
    RCC->APB1ENR1 |= RCC_APB1ENR1_I2C3EN;

    /* Make sure the peripheral is disabled for clock configuration */
    I2C3->CR1 &= ~I2C_CR1_PE;

    /* Timing register value is computed with the STM32CubeMX Tool,
    * Fast Mode @400kHz with I2CCLK = 80 MHz,
    * rise time = 100ns, fall time = 10ns
    * Timing Value = (uint32_t)0x00F02B86
    */
    I2C3->TIMINGR = (uint32_t) 0x00F02B86;

    /*clock stretching enabled by default*/




    /*Enable Dma requests for trans and recep*/
    I2C3->CR1 |= I2C_CR1_TXDMAEN ;

//    I2C3->CR2 |= I2C_CR2_AUTOEND;
    /*Enable Peripheral*/
    I2C3->CR1 |= I2C_CR1_PE;

    // Enable the 'transfer complete' I2C interrupt.
//    I2C3->CR1      |= I2C_CR1_TCIE;
    /*a STOP condition is automatically sent when NBYTES data are*/


}

void I2C::handle_master(uint8_t numberofBytes,uint8_t slaveaddress)
{
    /* Enable DMA1_Stream6*/
    DMA1_Channel2->CCR |= DMA_CCR_EN;

    /*Set slave @*/
    I2C3->CR2 |= slaveaddress << I2C_CR2_SADD_Pos;
    I2C3->CR2 |= I2C_CR2_AUTOEND;
    I2C3->CR2 |=  numberofBytes << I2C_CR2_NBYTES_Pos;
    I2C3->CR2 |= I2C_CR2_START;

}


