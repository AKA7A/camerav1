#include <mdhal/gpio/Gpio.hpp>
class I2C
{
public:

    void DMA_TX_Config(void);

    void DMA_Transmit(const uint8_t *pBuffer, uint8_t size);

    void i2c_slave_config(uint8_t saddress);

    void i2c_master_config();

    void handle_master(uint8_t numberofBytes, uint8_t slaveaddress);
};